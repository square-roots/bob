#include <stdio.h>
#include <math.h>

int main()
{
    // Declare variables
    double a;
    double b;
    double c;
    double d;
    double x1;
    double x2;
    double real;
    double imag;

    // Input coefficients
    printf("Enter the value of a: ");
    scanf("%lf", &a);
    printf("Enter the value of b: ");
    scanf("%lf",&b);
    printf("Enter the value of c: ");
    scanf("%lf", &c);

    // Calculate discriminant
    d = b * b - 4 * a * c;

    // Check discriminant and find roots
    if (d > 0) // Real and distinct roots
    {
        x1 = (-b + sqrt(d)) / (2 * a);
        x2 = (-b - sqrt(d)) / (2 * a);
        printf("The roots are %.2f and %.2f\n", x1, x2);
    }
    else if (d == 0) // Real and equal roots
    {
        x1 = x2 = -b / (2 * a);
        printf("The roots are %.2f and %.2f\n", x1, x2);
    }
    else // Complex and conjugate roots
    {
        real = -b / (2 * a);
        imag = sqrt(-d) / (2 * a);
        printf("The roots are %.2f + %.2fi and %.2f - %.2fi\n", real, imag, real, imag);
        printf("Complex");
    }

    return 0;
}
